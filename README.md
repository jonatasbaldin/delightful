<div align="center">
  <img src="https://codeberg.org/teaserbot-labs/delightful/raw/branch/main/assets/delightful-logo.png" alt="delightful"/>
</div>

<br/><br/>

<p align="center">
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delightful.md">What is a delightful list?</a></b>&nbsp;&nbsp;&nbsp;
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md">Creating a list</a></b>&nbsp;&nbsp;&nbsp;
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-checks.md">Quality checklist</a></b>&nbsp;&nbsp;&nbsp;
	<b><a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/faq.md">FAQ</a></b>
</p>

<br/>

---

<br/>

## Badge of honor

Please [delight us](delight-us.md) by launching your own curated list, and proudly wear this badge:&nbsp;&nbsp; ![delightful badge](assets/delightful-badge.png)

> **So collect your gems :gem: and fill those lists!**

## Contents

- [Delightful lists](#delightful-lists)

More information about this project:

- [A delightful tease](#a-delightful-tease)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Delightful lists

- [CLI software](https://codeberg.org/chaihron/delightful-cli/src/main/README.md) - CLI software for the console lovers.
- [Databases](https://codeberg.org/yarmo/delightful-databases/src/branch/main/README.md) - Databases in all shapes and sizes.
- [Funding](https://codeberg.org/teaserbot-labs/delightful-funding/src/branch/main/README.md) - Funding FOSS, Open Data, Open Science projects and sustainable businesses.
- [Gamification](https://codeberg.org/teaserbot-labs/delightful-gamification/src/branch/main/README.md) - Gamification and reputation system resources for developers.
- [Humane AI](https://codeberg.org/teaserbot-labs/delightful-humane-ai/src/branch/main/README.md) - Humane artificial intelligence resources that are open and accessible.
- [Humane design](https://codeberg.org/teaserbot-labs/delightful-humane-design/src/branch/main/README.md) - Humane design resources for UX designers and developers.
- [Linked data](https://codeberg.org/teaserbot-labs/delightful-linked-data/src/branch/main/README.md) - Linked data resources for developers.
- [Open science](https://codeberg.org/teaserbot-labs/delightful-open-science/src/branch/main/README.md) - Resources, organization and free software that support open science.
- [Sustainable business](https://codeberg.org/teaserbot-labs/delightful-sustainable-business/src/branch/main/README.md) - Sustainable business resources and exemplars of doing business online.

## A delightful tease

This repository sits in the organization [teaserbot labs™](https://codeberg.org/teaserbot-labs) which is in turn part of the [innercircles community](https://innercircles.community). The goal of this project is to gently tease and nudge people to use better software alternatives, and to value their freedom more deeply. More information is available in our [FAQ](faq.md#about-innercircles-community).

## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/yarmo/delightful-databases/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, the [maintainers](#maintainers) and other [contributors](delightful-contributors.md) have waived all copyright and related or neighboring rights to this work.

Many thanks to Sindre Sorhus for some [awesome](delightful.md#inspired-by-awesome-lists) inspiration, and to [Codeberg](https://codeberg.org) for providing us this space.
