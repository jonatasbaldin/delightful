# Frequently asked questions

## About this project

<details><summary><b>Who initiated the delightful project?</b></summary>
<p><br/>
  It is I, <a href="https://community.humanetech.com/u/aschrijver/summary">Arnold Schrijver</a>: small guy, huge fellow (well, in stature). I am sort of a neo-generalist techie that slowly became more and more disillusioned - worried even - by the promises of technology and societal trends regarding our collective future.

  In 2018 I got involved with the non-profit [Center for Humane Technology](https://humanetech.com) (CHT) think-tank. Based on their forum I initiated - and still facilitate as admin - the [Humane Tech Community](https://humanetech.community), a grassroots movement independent of the CHT (though still affiliated).

  I am in love ❤️ with the (ActivityPub-based) [fediverse](https://fediverse.party/), well, with the Decentralized Web in general, and - if time permits - intend to found an open community, called [innercircles](https://innercircles.community), that will develop even more applications for it. Hopefully then, you will join me in my efforts.
</p>
</details>

<details><summary><b>What triggered the delightful project idea?</b></summary>
<p><br/>
  Why was the idea for this project started, you ask? Well, it was triggered by an image search on DuckDuckGo for the term <i>'delightful'</i> that only returned <a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/assets/delightful-according-to-duckduckgo.jpg">photo's of freaking Louis Vuitton® bags</a> as the crazy commercial top results. WTF? What rabbithole has society been sucked into?

  Positive counter-action was required. The idea sprung up and was [tooted](https://mastodon.social/@humanetech/104083516228734656) to the fediverse. The delightful project was born.

  If this initiative is successful then in future maybe we'll find sweet Blender, Krita and other FOSS-made images and logo's when we do the same and similar searches.
</p>
</details>

<details><summary><b>What was the inspiration behind the concept?</b></summary>
<p><br/>
  For many years I - initiator of this project - have been using the <a href="https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delightful.md#inspired-by-awesome-lists">awesome</a> lists on Github for work and pleasure to find the coolest projects and information resources. I also maintain an awesome list myself: the <a href="https://github.com/humanetech-community/awesome-humane-tech">Awesome Humane Technology</a> list (which probably also qualifies as a delightful list, as it only contains open-source projects).

  However, the awesome project is less restrictive in what content it allows to exist in its curated lists. It can include commercial, non-free resources, and proprietary or even outright inhumane tech projects (e.g. a game that addicts kids with lootboxes and in-game purchases). This is where the delightful project differs. It serves to promote freedom and further humanity.

  Furthermore the word <i>'awesome'</i> in the meaning of 'awe inspiring' or 'cool' doesn't cut what we need technology to be. Our tech should become more gentle, more unobtrusive and in support of human values. Therefore <i>'delightful'</i> is a much more appropriate and, well.. delightful word.
</p>
</details>

<details><summary><b>What does the Chinese character in the badge mean?</b></summary>
<p><br/>
  The Unicode character <a href="https://en.wiktionary.org/wiki/%E6%82%A6#Chinese">悦</a> (Yuè) is the Simplified Chinese verb that means 'delight' or 'please' (according to Google Translate, but not exactly matched in <a href="https://www.freetranslations.org/#">free translations</a>). Taken together you could interpret it as <i>"Delight, please!"</i> which is very fitting for this crowdsourced project.
</p>
</details>

<br/>

## About innercircles community

<details><summary><b>What is this teaserbot labs™ organization this project belongs to?</b></summary>
<p><br/>
  While it sounds like the name of an enterprise, this is not about a commercial entity. Quite the contrary of that: this project and the organization are part of the open <a href="https://innercircles.community">innercircles community℠</a>.

  The company name of 'teaserbot labs™' constitutes a tease. The first tease - hopefully - of many more to come. Gentle teases and nudges for people who are apparently hypnotised by hypercapitalism. The goal is to show them better, alternative paths to take, and to have some fun when taking them.

  Paths that just happen to lead to a happier, brighter future. For the delightful project the path that is shown is that which leads to more prominent use of free software, more open science and freedom of information.

  You can all help in that teasing game. So please come, help us tease and nudge. **Let's play!**
</p>
</details>

<details><summary><b>What is innercircles community℠ then?</b></summary>
<p><br/>
  An open community - still incubating - based on an interesting concept: that simple solutions still exist. And they can be found in the common knowledge, mondane wisdom we found all around us. Ignored while we are rat-racing around. We even know these solutions already, but we just don't apply them, or forget. And we don't want to be taught, or told to use our common sense.

  So innercircles doesn't teach, or tell people what to do. We just play, and make it fun to join the game. Play and playfulness can be very rewarding - as our children know all too well - so we try to emphasize on that. Forget all the seriousness and problems. We have to learn how to play again, and play a different game this time.

  In innercircles we play the game of life, and strive to become better players than we were before. No grand visions, only delightful dreams.

  We adhere to these three mindful principles:

  1. **Success is irrelevant**, but every slight improvement counts.
  2. **Failure is irrelevant**, but for the opportunity to learn.
  3. **Timescale is irrelevant**, because only progress is our goal.

  Applied together these will free us from expectation, commitments and obligations, unless we set them for ourselves.

  Our public website is at [innercircles.community](https://innercircles.community), but contributors (circle builders) can find us soon at [center.innercircles.community](https://center.innercircles.community) (still 404 Not Found for a while though).
</p>
</details>